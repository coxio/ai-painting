# AI绘画 ✨

> 记录我使用 AI 绘画的过程，帮助更多的人学会 AI 画画，成为数字游民。

#### ✨ 持续更新中...

目前有大量的平台推出了 AI 绘画的能力，这里做一个汇总。有更多的欢迎前来补充 (可直接提交 pr)，也欢迎进群一起交流探索

## 一、可使用绘画的平台
·
### 1 国外

| Name                    | Tags               | URL                                                                                                            |
| ----------------------- | ------------------ | -------------------------------------------------------------------------------------------------------------- |
| midjourney              | 新用户免费20次     | https://www.midjourney.com                                                                                     |
| wombo.art               | 免费               | https://app.wombo.art                                                                                          |
| Google Colab            | 免费               | https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/stable_diffusion.ipynb      |
| DALL·E 2                | 排队申请           | https://openai.com/dall-e-2                                                                                    |
| artbreeder              | 免费               | https://www.artbreeder.com/beta/collage                                                                        |
| dreamstudio             | 200点数            | https://beta.dreamstudio.ai                                                                                    |
| nightcafe               | -                  | https://creator.nightcafe.studio/create/text-to-image?algo=stable                                              |
| starryai                | -                  | https://create.starryai.com/my-creations                                                                       |
| WebUI                   | 免费               | https://colab.research.google.com/github/altryne/sd-webui-colab/blob/main/Stable_Diffusion_WebUi_Altryne.ipynb |
| WebUI-AUTOMATIC1111版本 | 免费               | https://colab.research.google.com/drive/1Iy-xW9t1-OQWhb0hNxueGij8phCyluOh                                      |
| 替换图片                | 免费               | https://colab.research.google.com/drive/1R2HJvufacjy7GNrGCwgSE3LbQBk5qcS3?usp=sharing                          |
| 生成视频                | 免费               | https://github.com/THUDM/CogVideo                                                                              |
| PS插件-绘画生成图片     | -                  | https://www.nvidia.com/en-us/studio/canvas                                                                     |
| 3D模型                  | 免费               | https://colab.research.google.com/drive/1u5-zA330gbNGKVfXMW5e3cmllbfafNNB?usp=sharing                          |
| elbo                    | -                  | https://art.elbo.ai                                                                                            |
| deepdreamgenerator      | -                  | https://deepdreamgenerator.com                                                                                 |
| big-sleep               | 免费               | https://github.com/lucidrains/big-sleep                                                                        |
| nightcafe               | -                  | https://nightcafe.studio                                                                                       |
| craiyon                 | -                  | https://www.craiyon.com                                                                                        |
| novelai                 | -                  | https://novelai.net                                                                                            |
| novelai 免费版          | 免费               | https://github.com/JingShing/novelai-colab-ver                                                                 |
| Sd-Outpainting          | 免费               | https://github.com/lkwq007/stablediffusion-infinity                                                            |
| TyPaint                 | 免费               | https://apps.apple.com/us/app/typaint-you-type-ai-paints/id1624024392                                          |
| PicSo                   | 新用户每天免费10次 | https://picso.ai                                                                                               |
| sd-outpaing             | 免费               | https://github.com/lkwq007/stablediffusion-infinity                                                            |
| novelai-colab 版本      | 免费               | https://github.com/acheong08/Diffusion-ColabUI                                                                 |
| novelai-colab 版本2     | 免费               | https://github.com/JingShing/novelai-colab-ver                                                                 |

### 2 国内
| Name             | 价格              | URL                                                        |
| ---------------- | ----------------- | ---------------------------------------------------------- |
| 文心大模型       | 暂时免费          | https://wenxin.baidu.com/moduleApi/ernieVilg               |
| 文心-一格        | 暂时免费          | https://yige.baidu.com/#/                                  |
| 6pen             | 部分免费          | https://6pen.art                                           |
| 大画家Domo       | -                 | https://www.domo.cool                                      |
| PAI              | 免费              | https://artpai.xyz                                         |
| 爱作画           | 有免费次数 + 付费 | https://aizuohua.com                                       |
| 飞链云AI绘画版图 | 免费              | https://ai.feilianyun.cn                                   |
| Freehand意绘     | 免费              | https://freehand.yunwooo.com                               |
| 即时AI           | 免费              | https://js.design/pluginDetail?id=6322a4ab0eededcff6ba451a |
| 盗梦师           | 有免费次数 + 付费 | 微信小程序搜盗梦师                                         |
| 画几个画         | -                 | 微信小程序搜画几个画                                       |
| 智能画图         | 免费 + 看广告     | 微信小程序搜智能画图                                       |
| MuseArt          | 付费 + 看广告     | 微信小程序搜 MuseArt                                       |
| 意见AI绘画       | 有免费次数 + 付费 | 微信小程序搜意见AI绘画                                     |

### 3 模型下载

- [x] [novelAI](https://huggingface.co/acheong08/secretAI/resolve/main/stableckpt/animefull-final-pruned/model.ckpt)

- [x] [stable_diffusion_v1-5](https://huggingface.co/runwayml/stable-diffusion-v1-5/resolve/main/v1-5-pruned-emaonly.ckpt)

- [x] [stable_diffusion_v1-4](https://huggingface.co/CompVis/stable-diffusion-v-1-4-original/resolve/main/sd-v1-4.ckpt)

- [x] [waifu_diffusion](https://huggingface.co/hakurei/waifu-diffusion-v1-3/resolve/main/wd-v1-3-float32.ckpt)


## 二、使用教程

### 1. Stable Diffusion (推荐)
- [用Colab免费部署自己的AI绘画云平台 -  Stable Diffusion](https://mp.weixin.qq.com/s/2H1gCoOVBK89dIhEqoTQmA)

- [AI数字绘画 stable-diffusion 保姆级教程](https://mp.weixin.qq.com/s/nDnQuZn3hVgrwqWVada2cw)

### 2. Disco Diffusion
- [最简单全面本地运行Colab及Disco Diffusion教程](https://www.bilibili.com/read/cv16202697)

- [人工智能绘画工具 Disco Diffusion 入门教程](https://www.zcool.com.cn/article/ZMTM3OTg3Mg==.html)

- [一条录制的Disco Diffusion 生成器教程的内容](https://weibo.com/5519581673/LnZuxbAC8?type=repost)

- [堪比艺术家！被疯狂安利的 AI 插画神器 Disco Diffusion 有多强？](https://www.uisdc.com/disco-diffusion)

- [用AI如何画概念图？](https://www.shangyexinzhi.com/article/4648362.html)


## 三、自建教程

### 1. GPU场景推荐

- [https://www.autodl.com/createInstance](https://www.autodl.com/createInstance)

- [http://gpu.ai-galaxy.cn/store](http://gpu.ai-galaxy.cn/store)

### 2. 显卡选择
- [时代变了，大人：RTX 3090时代，哪款显卡配得上我的炼丹炉？](https://zhuanlan.zhihu.com/p/225507448)

### 3. 自建教程
- [人人都能用的「AI 作画」，如何把 Stable Diffusion 装进电脑？](https://mp.weixin.qq.com/s/jL4m4e-A1oc44Z8PLyvA2A)

- [https://github.com/fboulnois/stable-diffusion-docker](https://github.com/fboulnois/stable-diffusion-docker)

- [https://github.com/AbdBarho/stable-diffusion-webui-docker](https://github.com/AbdBarho/stable-diffusion-webui-docker)

### 4. MAC自建

- [stable-diffusion](https://replicate.com/blog/run-stable-diffusion-on-m1-mac)

- [diffusionbee-stable-diffusion-ui](https://github.com/divamgupta/diffusionbee-stable-diffusion-ui)


## 四、调参教程

### 1. NovelAI 专属

- [元素法典 - Novel AI 元素魔法全收录](https://docs.qq.com/doc/DWHl3am5Zb05QbGVs)

- http://wolfchen.top/tag/

- https://aitag.top/  

- https://tags.novelai.dev/

- [NovelAI 法术解析](https://spell.novelai.dev/)

### 2. Disco Diffusion 关键词

- [Disco Diffusion](https://www.notion.so/Disco-Diffusion-d8a78d7a5a8b40238da820687615dee6) 

### 3. Stable Diffusion 关键词

- [arthub.ai](https://arthub.ai/)

- [lexica.art](https://lexica.art/) 

- [krea.ai](https://www.krea.ai/?continueFlag=6591d07b3186f4c7e58de1a4bcfaefb0)

- [stable-diffusion-prompt-builder](https://promptomania.com/stable-diffusion-prompt-builder/)

### 4. Midjourney 关键词

- [prompt.noonshot.com/midjourney](https://prompt.noonshot.com/midjourney)

- [huggingface.co/spaces/doevent/prompt-generator](https://huggingface.co/spaces/doevent/prompt-generator)

- [midjourney-prompt-helper.netlify.app](https://midjourney-prompt-helper.netlify.app/)

### 5. 法术解析

- https://spell.novelai.dev/
- [Deep Danbooru](http://dev.kanotype.net:8003/deepdanbooru/)


## 五、其他

- [img2img记录](https://www.notion.so/img2img-f3ef70c4b67c49d1b7a15fca91955eaa)

- [AI视频](https://www.notion.so/AI-2b14076b2b1e4ef9b68a68924f716905)

- [Koishi.js 聊天机器人 NovelAI 插件](https://bot.novelai.dev/)

- [将 pt 格式的训练模型文件转换为 png 格式](https://colab.research.google.com/gist/wfjsw/2b2a26349bef1ce891f6ab4d4fb3030a/convert-pt-embedding-to-png.ipynb)


## 六、微信交流群

- **微信群：可加 aigc2022 拉你入群**

## 七、特别鸣谢
- https://github.com/hua1995116/awesome-ai-painting